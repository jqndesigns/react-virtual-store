# Sobre la aplicación

Esta aplicación es una demostración que simula una tienda donde puedes comprar dispositivos móviles. Consta de dos vistas, la primera mostrando el listado de dispositivos disponibles y la segunda siendo el detalle de cada uno de dichos dispositivos cuando haces clic en uno de ellos.

# IMPORTANTE

En el momento en que empecé a desarrollar la aplicación la API sugerida para utilizar como proveedor estaba caída, por lo que creé mi propio Backend con una API mockeada en Express. Tanto el FRONT como el BACK forman parte de este único repositorio.

# Cómo instalar y ejecutar

Clona este repositorio y ejecuta el comando `npm i` tanto en la carpeta de 'back' como de 'front'
Para arrancar de forma concurrente el back y el front ejecuta el comando `npm start` en la carpeta 'front'.