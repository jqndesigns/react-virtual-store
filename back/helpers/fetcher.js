const fetch = require('node-fetch');

const makeFetch = async (url) => {
  //This API is not using a real fetch system, but I keep this basic fetch code
  console.log(url);

  let body;

  try {
    const response = await fetch(url);
    body = await response.json();
  } catch (error) {
    body = { err: error }
  } finally {
    return body;
  }

}

const sendResponse = (body, res) => {
  let status = body.err ? body.status : 200;
  res.status(status).json(body);
}

module.exports = {
  makeFetch,
  sendResponse
};