const { Router } = require('express');
const { getAllproducts, getProductDetail, addProductToCart } = require('../controllers/productController');
const router = Router();

router.get('/', getAllproducts);

router.get('/:id', getProductDetail);

router.post('/', addProductToCart)

module.exports = router;