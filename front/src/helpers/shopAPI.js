import { setWithExpiry, getWithExpiry } from './localStorage';

const baseUrl = `${process.env.REACT_APP_API_URL}`;
const expireTtl = 60000;

export const fetchProducts = async () => {
  let products = getWithExpiry('allProducts');

  if (products === null) {
    const response = await fetch(`${baseUrl}/product`, {
      cors: 'no-cors',
    });
    products = await response.json();
  }

  setWithExpiry('allProducts', products, expireTtl);
  return [...products];
};

export const fetchProductDetail = async (productId) => {
  let product = getWithExpiry(`product${productId}`);

  if (product === null) {
    const response = await fetch(`${baseUrl}/product/${productId}`, {
      cors: 'no-cors',
    });
    product = await response.json();
  }

  setWithExpiry(`product${productId}`, product, expireTtl);
  return product;
};

export const addItem = async (color, memory, id) => {
  const body = {
    color,
    id,
    memory,
  };

  const response = await fetch(`${baseUrl}/product`, {
    body: JSON.stringify(body),
    cors: 'no-cors',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    method: 'POST',
  });

  const res = await response.json();

  return structuredClone(res);
};