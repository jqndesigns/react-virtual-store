export const fieldHasError = (field) =>
  field.validations.isRequired &&
  (field.value === 0 || field.value === '' || field.value === '0');
