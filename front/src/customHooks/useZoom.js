import { useState } from 'react';

export const useZoom = () => {
  const [usingZoom, setUsingZoom] = useState(false);

  const applyZoom = (time) => {
    setUsingZoom(true);
    setTimeout(() => {
      setUsingZoom(false);
    }, time);
  };

  return [usingZoom, applyZoom];
};
