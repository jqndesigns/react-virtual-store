import { useState } from 'react';

import { fieldHasError } from '../helpers/formValidator';

const defaultState = [
  {
    fieldName: 'field',
    hasError: false,
    validate: true,
    validations: {
      isRequired: false,
    },
    value: 0,
  },
];

export const useForm = (initialState = defaultState) => {
  const [values, setValues] = useState(initialState);

  const reset = (newState = initialState) => {
    setValues(newState);
  };

  const handleInputChange = ({ target }) => {
    setValues({
      ...values,
      [target.name]: {
        ...values[target.name],
        value: target.value,
      },
    });
  };

  const handleInputError = () => {
    const newValues = structuredClone(values);
    let errors = false;

    Object.keys(newValues).forEach((field) => {
      if (values[field].validate) {
        newValues[field].hasError = false;
        if (fieldHasError(values[field])) {
          newValues[field].hasError = true;
          errors = true;
        }
      }
    });

    setValues(newValues);
    return errors;
  };

  return [values, handleInputChange, handleInputError, reset];
};
