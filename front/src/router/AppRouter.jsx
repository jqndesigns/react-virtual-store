import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Navigate, Routes } from 'react-router-dom';

import App from '../App';
import { store } from '../app/store';
import DetailsView from '../views/DetailsView';
import ListView from '../views/ListView';

export const AppRouter = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route element={<App />} path="/*">
          <Route element={<ListView />} path="list" />
          <Route element={<DetailsView />} path="product/:id" />
          <Route element={<Navigate to="/list" />} path="*" />
        </Route>
      </Routes>
    </BrowserRouter>
  </Provider>
);
