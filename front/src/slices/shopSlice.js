import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { getWithExpiry } from '../helpers/localStorage';
import { fetchProducts } from '../helpers/shopAPI';

const initialState = {
  cart: getWithExpiry('cartCount') || 0,
  items: [],
  status: 'idle',
};

export const fetchItems = createAsyncThunk('shop/fetchProducts', async () => {
  const products = await fetchProducts();
  return products;
});

export const shopSlice = createSlice({
  extraReducers: (builder) => {
    builder
      .addCase(fetchItems.pending, (state) => {
        state.status = 'loading';
      })
      .addCase(fetchItems.fulfilled, (state, action) => {
        state.status = 'idle';
        state.items = [...action.payload];
      });
  },
  initialState,
  name: 'shop',
  reducers: {
    addItemToCart: (state) => {
      state.cart += 1;
    },
  },
});

export const { addItemToCart } = shopSlice.actions;

export const getItems = (state) => state.shop.items;
export const getCart = (state) => state.shop.cart;
export const getStatus = (state) => state.shop.status;

export default shopSlice.reducer;
