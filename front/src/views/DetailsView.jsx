import React, { useEffect, useState } from 'react';
import { Navigate, useParams } from 'react-router-dom';

import Actions from '../components/Actions';
import Description from '../components/Description';
import Image from '../components/Image';
import { fetchProductDetail } from '../helpers/shopAPI';

const DetailsView = () => {
  const { id } = useParams();

  const [loading, setLoading] = useState(true);
  const [item, setItem] = useState({});

  useEffect(() => {
    const fetchData = async () => {
      setItem(await fetchProductDetail(id));
      setLoading(false);
    };

    fetchData();
  }, []);

  return (
    <>
      {(id <= 0 || Number.isNaN(+id)) && <Navigate to="/" />}

      <div id="itemDetail">
        {loading && <h1>Loading...</h1>}
        {!loading && (
          <div className="container">
            <div className="container__item">
              <Image desc={`${item.brand}: ${item.model}`} source={item.img} />
            </div>
            <div className="container__item container__vertical">
              <Description item={item} />
              <Actions item={item} />
            </div>
          </div>
        )}
      </div>
    </>
  );
};

export default DetailsView;
