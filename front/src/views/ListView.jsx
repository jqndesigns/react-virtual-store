import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import Item from '../components/Item';
import Search from '../components/Search';
import { getItems, fetchItems, getStatus } from '../slices/shopSlice';

const ListView = () => {
  const items = useSelector(getItems);
  const loading = useSelector(getStatus);

  const [filteredItems, setFilteredItems] = useState([...items]);
  const dispatch = useDispatch();

  useEffect(() => {
    if (items.length === 0) {
      dispatch(fetchItems());
    }
  }, []);

  useEffect(() => {
    setFilteredItems([...items]);
  }, [items]);

  const filterItems = (searchQ) => {
    if (searchQ.length > 1) {
      const newFilteredItems = items.filter(
        (i) =>
          i.model.toUpperCase().includes(searchQ.toUpperCase()) ||
          i.brand.toUpperCase().includes(searchQ.toUpperCase()),
      );
      setFilteredItems([...newFilteredItems]);
    } else if (searchQ.length === 0) {
      setFilteredItems([...items]);
    } else {
      setFilteredItems([]);
    }
  };

  return (
    <>
      <Search filterItems={filterItems} />
      <div id="itemList">
        {loading === 'loading' && <h1>Loading...</h1>}
        {filteredItems.map((i) => (
          <Item key={i.id} itemData={i} />
        ))}
      </div>
    </>
  );
};

export default ListView;
