import React from 'react';
import { Outlet } from 'react-router-dom';

import Header from './components/Header';

import './styles/styles.scss';

function App() {
  return (
    <>
      <Header />     
      <Outlet />
    </>
  );
}

export default App;
