/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Item = ({ itemData }) => {
  const { brand, id, img, model, price } = itemData;

  return (
    <Link to={`/product/${id}`}>
      <div className="listItem">
        <img alt={`Item ${brand} ${model}`} src={img} />
        <div className="listItem__details">
          <span>
            <b>{brand}</b> {model}
          </span>
          <span className="listItem__details__price highlightBackground">{price} EUR</span>
        </div>
      </div>
    </Link>
  );
};

Item.propTypes = {
  itemData: PropTypes.object.isRequired,
};

export default Item;
