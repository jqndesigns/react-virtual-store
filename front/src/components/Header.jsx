import classNames from 'classnames';
import React, { useEffect } from 'react';
import { MdShop } from 'react-icons/md';
import { useSelector } from 'react-redux';
import { Link, useLocation } from 'react-router-dom';

import { useZoom } from '../customHooks/useZoom';
import { setWithExpiry } from '../helpers/localStorage';
import { getCart } from '../slices/shopSlice';

const Header = () => {
  const location = useLocation();
  const itemCount = useSelector(getCart);
  const expireTtl = 60000;

  const [usingZoom, applyZoom] = useZoom();

  useEffect(() => {
    if (itemCount === 0) {
      return;
    }

    setWithExpiry('cartCount', itemCount, expireTtl);
    applyZoom(200);
  }, [itemCount]);

  const cartClasses = classNames({
    "itemCount": true,
    "zoom": usingZoom
  })

  return (
    <div id="header">
      <div className="title">
        <Link to="/">Virtual Store</Link>
      </div>
      <div className="breadcrumb">
        {location.pathname.includes('product') && (
          <Link to="/list">{`> List`}</Link>
        )}
        <Link to={location.pathname}>
          {location.pathname.replaceAll('/', ' > ')}
        </Link>
      </div>
      <div className={cartClasses}>
        {itemCount}
        <MdShop />
      </div>
    </div>
  );
};

export default Header;
