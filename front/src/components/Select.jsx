/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Select = ({ defaultText, field, onChangeHandler, values }) => {
  const classes = classNames({
    error: field.hasError,
  });

  return (
    <select
      className={classes}
      id={field.fieldName}
      name={field.fieldName}
      value={field.value}
      onChange={onChangeHandler}
    >
      <option key={0} value="0">
        {defaultText}
      </option>
      {values.map((v) => (
        <option key={v.id} value={v.id}>
          {v.value}
        </option>
      ))}
    </select>
  );
};

Select.propTypes = {
  defaultText: PropTypes.string,
  field: PropTypes.object.isRequired,
  onChangeHandler: PropTypes.func.isRequired,
  values: PropTypes.array.isRequired,
};

Select.defaultProps = {
  defaultText: 'Selecciona...',
};

export default Select;
