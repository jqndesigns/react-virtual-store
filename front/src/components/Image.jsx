import React from 'react';
import PropTypes from 'prop-types';

const Image = ({
  desc = 'No image found for this product',
  source = 'https://upload.wikimedia.org/wikipedia/commons/3/3f/Placeholder_view_vector.svg',
}) => <img alt={desc} src={source} />;

Image.propTypes = {
  desc: PropTypes.string.isRequired,
  source: PropTypes.string.isRequired,
};

export default Image;
