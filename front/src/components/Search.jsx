import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Search = ({ filterItems }) => {
  const [searchValue, setSearchValue] = useState('');

  const handleSearch = (e) => {
    const { value } = e.target;
    setSearchValue(value);
    filterItems(value);
  };

  return (
    <div id="searchBar">
      <input
        autoComplete="off"
        placeholder="Busca..."
        type="text"
        value={searchValue}
        onChange={handleSearch}
      />
    </div>
  );
};

Search.propTypes = {
  filterItems: PropTypes.func.isRequired,
};

export default Search;
