/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';

const Description = ({ item }) => (
  <div className="container__vertical__item">
    <h3>
      {item.brand} {item.model}
    </h3>
    <div>
      <span>Precio</span> {item.price} €
    </div>
    <div>
      <span>SO</span> {item.specs.so}
    </div>
    <div>
      <span>CPU</span> {item.specs.cpu}
    </div>
    <div>
      <span>RAM</span> {item.specs.ram}
    </div>
    <div>
      <span>Cámaras</span> {item.specs.cam}
    </div>
    <div>
      <span>Tamaño</span> {item.specs.dimension}
    </div>
    <div>
      <span>Peso</span> {item.specs.weight}
    </div>
  </div>
);

Description.propTypes = {
  item: PropTypes.object.isRequired,
};

export default Description;
