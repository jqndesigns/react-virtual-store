/* eslint-disable react/forbid-prop-types */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import { addItem } from '../helpers/shopAPI';
import { addItemToCart } from '../slices/shopSlice';
import Select from './Select';
import { useForm } from '../customHooks/useForm';

const Actions = ({ item }) => {
  const [sending, setSending] = useState(false);

  const [formValues, setFormValues, handleInputError] = useForm({
    colorSelect: {
      fieldName: 'colorSelect',
      hasError: false,
      validate: item.colors !== undefined,
      validations: {
        isRequired: true,
      },
      value: 0,
    },
    memorySelect: {
      fieldName: 'memorySelect',
      hasError: false,
      validate: item.memory !== undefined,
      validations: {
        isRequired: true,
      },
      value: 0,
    },
  });

  const { colorSelect, memorySelect } = formValues;
  const dispatch = useDispatch();

  const handleSubmit = async () => {
    if (sending) {
      return;
    }

    if (handleInputError()) {
      return;
    }

    setSending(true);

    try {
      const { response } = await addItem(
        +colorSelect.value,
        +memorySelect.value,
        item.id,
      );
      if (response === 'OK') {
        dispatch(addItemToCart());
      }
    } catch (e) {
      throw new Error(e);
    } finally {
      setSending(false);
    }
  };

  return (
    <div className="container__vertical__item">
      <h4>Por favor, selecciona tu modelo</h4>

      {item.colors && (
        <Select
          defaultText="Selecciona color..."
          field={colorSelect}
          values={item.colors.map((i) => ({
            id: i.colorId,
            value: i.colorName,
          }))}
          onChangeHandler={setFormValues}
        />
      )}

      {item.memory && (
        <Select
          defaultText="Selecciona capacidad..."
          field={memorySelect}
          values={item.memory.map((i) => ({
            id: i.memoryId,
            value: i.memoryCapacity,
          }))}
          onChangeHandler={setFormValues}
        />
      )}

      {!sending && (
        <button type="submit" onClick={handleSubmit}>
          Añadir
        </button>
      )}
      {sending && <h3>Enviando...</h3>}
    </div>
  );
};

Actions.propTypes = {
  item: PropTypes.object.isRequired,
};

export default Actions;
